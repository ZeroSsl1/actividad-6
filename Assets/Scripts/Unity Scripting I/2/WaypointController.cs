﻿using UnityEngine;

public class WaypointController : MonoBehaviour {

    private GameObject[] waypoints;          // Array with child Transforms
    private int currentWayPointIndex = 0; 
    private PlayerController player;         // Reference to PlayerController

    void Start() {
        // Get GameObjects by TagName
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");

        if (waypoints == null) {
            Debug.Log("No waypoints found in the scene");
            enabled = false;
            return;
        }

        // Get components through GameObject Name
        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

        if (!player) {
            Debug.Log("No GameObject found with Player Tag");
            enabled = false;
            return;
        }
    }
	
	void Update () {
        // When player position is equal to waypoint position, move to next waypoint
        if (player.transform.position.Equals(waypoints[currentWayPointIndex].transform.position)) {
            currentWayPointIndex++;

            // If the player reach the last waypoint, go to first wayPoint again
            if (currentWayPointIndex >= waypoints.Length)
                currentWayPointIndex = 0;
        }
    }

    // Returns the currentWaypoint position
    public Vector3 getCurrentWaypointPos() {
        return waypoints[currentWayPointIndex].transform.position;
    }
}



