﻿using UnityEngine;

public class RaycastExample : MonoBehaviour {

    [Range(0.01f, 250)]
    public float distance = 100;

    void FixedUpdate() {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        
        if (Physics.Raycast(transform.position, fwd, out hit, distance))
            print(hit.transform.gameObject.name + " is in front of the camera");
    }
}

