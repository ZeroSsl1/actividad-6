﻿using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objectToSpawn;
    public GameObject spawnObject;
    public float maxTime = 3;
    public float minTime = 2;
    private float time;
    private float spawnTime;
    [Range(1.0f,10.0f)]
    public float MinScale;
    [Range(1.0f, 10.0f)]    
    public float MaxScale;
    public float R;
    public float G;
    public float B;
    void Start()
    {
        time = minTime;
    }
    void Update () {

        if (Input.GetKeyDown(KeyCode.S)) {

            GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(MinScale, MaxScale);
            new Color (R,G,B);
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
            rb.AddForce(Vector3.right * Random.Range(10, 150), ForceMode.Impulse);
            time += Time.deltaTime;
            }
	}
}

