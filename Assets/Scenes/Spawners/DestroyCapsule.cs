﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCapsule : MonoBehaviour {

	void OnCollisionEnter (Collision collision) {
		if(collision.gameObject.name == "Prefab")
        {
            Destroy(gameObject);
        }
	}
}