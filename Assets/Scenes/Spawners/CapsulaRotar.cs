﻿using UnityEngine;
using System.Collections;

public class CapsulaRotar : MonoBehaviour
{

    GameObject cube;
    public Transform center;
    private Vector3 axis = Vector3.up;
    private Vector3 desiredPosition;
    public float radius = 2.0f;
    private float radiusSpeed = 0.5f;
    private float rotationSpeed = 80.0f;

    void Start()
    {
        transform.position = (transform.position - center.position).normalized * radius + center.position;
        radius = 2.0f;
    }

    void Update()
    {
        transform.RotateAround(center.position, axis, rotationSpeed * Time.deltaTime);
        desiredPosition = (transform.position - center.position).normalized * radius + center.position;
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
    }
}