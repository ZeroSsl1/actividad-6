﻿using UnityEngine;
using System.Collections;

public class CapsuleMovement : MonoBehaviour
{
    public float speed = 10.0F;
    public float speedH = 2.0f;
    public float speedV = 2.0f;
    private float H = 0.0f;
    private float V = 0.0f;
    void Update()
    {
        float vertical = Input.GetAxis("Vertical") * speed;
        float horizontal = Input.GetAxis("Horizontal") * speed;
        H += speedH * Input.GetAxis("Mouse X");
        V -= speedV * Input.GetAxis("Mouse Y");
        horizontal *= Time.deltaTime;
        vertical *= Time.deltaTime;
        transform.Translate(horizontal, 0, vertical);
        transform.eulerAngles = new Vector3(V, H, 0.0f);
    }
}